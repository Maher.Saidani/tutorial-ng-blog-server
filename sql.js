const Sequelize = require("sequelize");
const crypto = require("crypto");

const sequelize = new Sequelize("ngblog", "root", "root", {
    host: "localhost",
    dialect: "mariadb",
    port: 3306,
    dialectOptions: {
        timezone: process.env.db_timezone 
    }
});

const User = sequelize.define('user', {
    name: {type: Sequelize.STRING, allowNull: false},
    passwort: {type: Sequelize.STRING, allowNull: false},
    salt: {type: Sequelize.STRING, allowNull: false}
});

const Article = sequelize.define('article', {
    title: {type: Sequelize.STRING},
    key: {type: Sequelize.STRING},
    date: {type: Sequelize.DATE},
    description: {type: Sequelize.TEXT},
    content: {type: Sequelize.TEXT},
    imageUrl: {type: Sequelize.STRING},
    viewCount: {type: Sequelize.INTEGER},
    published: {type: Sequelize.BOOLEAN}
});

init = function(){
    sequelize
    .authenticate()
    .then(() => {
        console.log("connection has been established successfully.");
    })
    .catch(err => {
        console.error("unable to connect to the database: ", err);
    });

    Article.sync({force: true}).then(() => {

        Article.create({
            title: 'Article 1',
            key: 'a1',
            date: new Date(),
            content: '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>',
            description: 'Its a great to read this.. it means that the user can see the first content!',
            imageUrl: 'http://angular.io/assets/images/logos/angular/angular.png',
            published: true
        });

        Article.create({
            title: 'Article 2',
            key: 'a2',
            date: new Date(),
            content: '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>',
            description: 'Its a great to read this.. it means that the user can see the second content!',
            imageUrl: 'http://angular.io/assets/images/logos/angular/angular_solidBlack.png',
            published: false
        });

        Article.create({
            title: 'Article 3',
            key: 'a3',
            date: new Date(),
            content: '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>'+
            '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>',
            description: 'Its a great to read this.. it means that the user can see the third content!',
            imageUrl: 'http://angular.io/assets/images/logos/angular/angular.png',
            published: true
        });
    });

    User.sync();
};

getArticles = function(callback){
    Article.findAll({
        order: sequelize.literal("DATE DESC"),
        where: {published: true}})
    .then(articles => callback(articles));
};

getArticleByKey = function(options, callback){
    Article.findOne({where: {key: options.key, published: true}}).then(article => {
        if (article != null){
            article.update({
                viewCount: ++article.viewCount
            });
        }
        callback(article);
    });
};

getDashboardArticles = function(callback){
    Article.findAll({order: sequelize.literal("DATE DESC")}).then(articles => callback(articles));
};

updateArticlePublishState = function(request, callback){
    Article.findOne({where: {id: request.id}}).then(article => {
        if(article != null){
            article.update({
                published: request.published 
            });
        }
        callback(article);
    })
};

updateArticle = function(request, callback){
    Article.findOne({where: {id: request.id}}).then(article => {
        if(article != null){
            article.update({
                title: request.title,
                key: request.key,
                date: request.date,
                imageUrl: request.imageUrl,
                description: request.description,
                content: request.content
            });
        }
        callback(article);
    })
};

getDashboardArticleByKey = function(key, callback){
    Article.findOne({where: {key: key}}).then(article => callback(article));
};

deleteArticle = function(id, callback){
    Article.findOne({where: {id: id}}).then(
        article => {
            if (article != null){
                article.destroy().then(
                    deletedArticle => {callback(deletedArticle);}
                );
            } else {
                callback(null);
            }
        }
    );
};


createArticle = function(body, callback){
    Article.create({
        title: body.title,
        key: body.key,
        date: body.date,
        imageUrl: body.imageUrl,
        description: body.description,
        content: body.content
    }).then(article => callback(article));
};

addUser = function(user, callback){
    User.create({
        name: user.name.toLowerCase(),
        passwort: user.passwort,
        salt: user.salt
    }).then(callback(true));
};

loginUser = function(request, callback){
    User.findOne({where: {name: request.name}}).then(
        user => {
            if(user !== null){
                var passwortHash = crypto.pbkdf2Sync(request.passwort, user.salt, 1000, 64, "sha512").toString("hex");
                if ( user.passwort === passwortHash ){
                    callback(true);
                    return;
                }
            }

            callback(false);
        }
    );
};

module.exports.init = init;
module.exports.getArticles = getArticles;
module.exports.getArticleByKey = getArticleByKey;
module.exports.getDashboardArticles = getDashboardArticles;
module.exports.updateArticlePublishState = updateArticlePublishState;
module.exports.getDashboardArticleByKey = getDashboardArticleByKey;
module.exports.updateArticle = updateArticle;
module.exports.deleteArticle = deleteArticle;
module.exports.createArticle = createArticle;
module.exports.addUser = addUser;
module.exports.loginUser = loginUser;