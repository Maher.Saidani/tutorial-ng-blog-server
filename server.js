const express = require("express");
const cors = require("cors");
const sql = require("./sql");
const bodyParser = require("body-parser");

const app = express();

var corsOptions = {
    origin: [ "http://localhost:4200" ]
}

app.use(cors(corsOptions));
app.use(bodyParser.json());

app.listen(8000,() => {
    console.log("Server started and listening.");
    sql.init();
});

app.get("/", function(request, response){
    response.send("Hi :) server listening..");
});

require("./articles.js")(app, sql); // so that the server knows article.js and sql.js
require("./dashboard.js")(app, sql); // so that the server knows article.js and sql.js
require("./auth.js")(app, sql); // so that the server knows article.js and sql.js



