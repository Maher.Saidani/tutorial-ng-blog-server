//const ARTICLES = require("./mock-articles");


module.exports = function(app, sql){
    
    app.get("/articles", function(request, response){
        //response.send(ARTICLES);
        sql.getArticles(function(result){
            response.send(result);
        });
    })

    app.get("/articles/:key", function(request, response){
        //response.send(ARTICLES.filter( article => article.key === request.params.key)[0]);
        sql.getArticleByKey({key: request.params.key}, function(result){
            response.send(result);
        });
    })
};

