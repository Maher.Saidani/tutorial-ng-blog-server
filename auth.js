module.exports = function(app, sql){

    const crypto = require("crypto"); // library Crypto to hash password

    app.post("/user/register", function(request, response){
        request.body.salt = crypto.randomBytes(16).toString("hex");

        var passwordHash = crypto.pbkdf2Sync(request.body.passwort, request.body.salt, 1000, 64, "sha512").toString("hex");
        request.body.passwort = passwordHash;

        sql.addUser(request.body, result => response.send(result));

    });

    app.post("/user/login", function(request, response){
        const name= request.body.name;
        const passwort= request.body.passwort;
        sql.loginUser({name, passwort}, function(result){
            response.send(result);
        });
    });

};